By default the scripts will store the generated data in this subdirectory. 

In the folder plot_data_paper, the data that is plotted in the figures can be found for the respective models. 

In the folder raw_data_paper, the raw data used for the plots in the manuscript as output by the algorithm can be found for the respective models. 
This includes a dictionary of optimization parameters and optimal basis.